import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Container} from '@material-ui/core';
import * as serviceWorker from './serviceWorker';

//import apollo related packages
import { 
  ApolloClient,
  InMemoryCache, 
  NormalizedCacheObject, 
  ApolloProvider
} from '@apollo/client';


const client: ApolloClient<NormalizedCacheObject>= new ApolloClient({
  uri: 'https://news-reader.stagnationlab.dev/graphql',
  cache: new InMemoryCache()
});

ReactDOM.render(
  <Container>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </Container>
  ,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
