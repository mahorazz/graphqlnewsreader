import React from 'react';
//import logo from './logo.svg';
import './App.css';
import ArticleList from './components/listArticle/listArticles';

function App() {
  return (
    <div className="App">
      {ArticleList({limit: 5 ,skip: 0})}
    </div>
  );
}

export default App;
