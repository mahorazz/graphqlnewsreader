import React from 'react';
import { gql, useQuery} from '@apollo/client';
import {Grid, Typography} from '@material-ui/core';
import {Card, CardMedia, CardContent} from '@material-ui/core';

import './listArticles.css';

export interface articleListProps {
    limit: Number,
    skip: Number
}
interface Article {
    id : number
    title: string
    img: string
}

interface ArticlesList{
    newsList: {
        rows: Article[]
    }
}

const GET_ARTICLES = gql`
    query($limit: Int!, $skip: Int!){
    newsList(limit: $limit, skip:$skip){
        rows{
        id
        title
        img
        }
    }
    }
`;



const ListArticles = (props: articleListProps) => {
    
    const { loading, error, data } = useQuery<ArticlesList>(GET_ARTICLES, {
        variables: {limit: props.limit, skip: props.skip}
    });

    if (loading) return <h3> Loading </h3>;
    if (error) return `<h3>Error</h3> ${error}`;
    return (
        <Grid container>
            {data && data.newsList.rows.map( article =>(
                <Grid item key={article.id}>
                    <Card className="card">
                        <CardMedia className="cardMedia"
                            image = {article.img}
                        />
                        <CardContent>
                            <Typography variant="h5" gutterBottom>
                                {article.title}
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
            ))}
        </Grid>
    );

};

export default ListArticles;